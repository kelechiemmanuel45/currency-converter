
// install new service worker when ok, then reload page.
self.addEventListener("message", msg =>{
    if (msg.data.action==='skipWaiting'){
        self.skipWaiting()
    }
})

let CACHE_NAME = "mycache_v1";
let PRE_CACHE = ["/index.html"];//cache home

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                return cache.addAll(PRE_CACHE);
            })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.open(CACHE_NAME).then(function(cache) {
            return cache.match(event.request).then(function (response) {
                return response || fetch(event.request).then(function(response) {
                    cache.put(event.request, response.clone());
                    return response;
                }).catch(err => {
                    return fetch("/index.html")
                });
            });
        })
    );
});
