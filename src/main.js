import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import vuetify from './plugins/vuetify';
import 'bootstrap/dist/css/bootstrap.min.css'
import VueAlertify from 'vue-alertify';
import VueSocialSharing from 'vue-social-sharing'

Vue.use(VueSocialSharing);

Vue.use(VueAlertify,{
  notifier:{
    delay:1,
    position:'top-right'
  }
});
Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
