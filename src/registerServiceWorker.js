/* eslint-disable no-console */
import {register} from 'register-service-worker'
import Alertify from "alertifyjs";


const notifyUserAboutUpdate = worker => {
    Alertify.alert("Notification", "An update has been applied to this app",
        () => {
            worker.postMessage({action: "skipWaiting"});
            Alertify.success('Update Verified!')
        }
    );
};

if (process.env.NODE_ENV === 'production') {
    register(`service-worker.js`, {
        ready() {
            /*console.log(
                'App is being served from cache by a service worker.\n' +
                'For more details, visit https://goo.gl/AFskqB'
            )*/
        },
        registered() {
            // console.log('Service worker has been registered.')
        },
        cached() {
            // console.log('Content has been cached for offline use.')
        },
        updatefound() {
            // console.log('New content is downloading.')
        },
        updated(registration) {
            // console.log('New content is available; please refresh.');
            notifyUserAboutUpdate(registration.waiting);
        },
        offline() {
            // console.log('No internet connection found. App is running in offline mode.')
        },
        error(error) {
            // console.error('Error during service worker registration:', error)
        }
    })

    let refreshing;

    navigator.serviceWorker.addEventListener("controllerchange", () => {
        if (refreshing) return
        window.location.reload();
        refreshing = true
    })

}
